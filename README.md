
## What is included in the open source client

The project in this repository contains the Wire for Android client project. You can build the project yourself. However, there are some differences with the binary Wire client available on the Play Store. 
These differences are:
- the open source project does not include the API keys of YouTube, Localytics, HockeyApp and other 3rd party services.
- the open source project links against the open source Wire audio-video-signaling (AVS) library. The binary Play Store client links against an AVS version that contains proprietary improvements for the call quality.

## Prerequisites
In order to build Wire for Android locally, it is necessary to install the following tools on the local machine:
- JDK 8
- Android SDK

## How to build locally
1. Check out the wire-android repository. 
2. From the checkout folder, run `./gradlew assembleProdRelease`. This will pull in all the necessary dependencies from Maven.

These steps will build only the Wire client UI, pulling in all other Wire frameworks from Maven. If you want to modify the source/debug other Wire frameworks, you can check project dependencies and build other wire projects separately. The most interesting projects to check are: 

- [Sync Engine](https://github.com/wireapp/wire-android-sync-engine)
- [Audio Video Signaling](https://github.com/wireapp/avs)
- [generic-message-proto](https://github.com/wireapp/generic-message-proto)
- [wiretranslations-android](https://github.com/wireapp/wiretranslations-android)

## Android Studio
When importing project in Android Studio **do not allow** gradle plugin update. Our build setup requires Android Plugin for Gradle version 1.5. 
